const express = require('express');
const bodyParser = require('body-parser');
var app = express();
const request = require('request');
var path = require('path');
const mongoDB = require('./Src/controller');
mongoDB.connect();

let data = require('./Src/database');

require('dotenv').config()

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}))

app.listen(process.env.PORT || 5000, () => {
    console.log("App started Port 5000");
});

app.get("/", (req, res) => {
    res.send("Server Started!!!!")
})



app.post('/isDate', async (req, res) => {

    //************ Getting Current Day */
    let currentDay = new Date().getDate();
    console.log("Current Day \n", currentDay);

    //********* Checking if Number is Prime or Not */
    let checkPrime = await isPrime(currentDay);

    console.log("Prime Checker Result \n", checkPrime);

    if (checkPrime) {
        let cName = req.body.c_name || process.env.C_NAME;

        console.log("Company Name \n", cName);

        let reqUrl = process.env.URL + cName + "&appid=" + process.env.API_KEY

        //********** Getting Weather Data using OpenWeatherAPI */
        request(reqUrl, async function (err, response, body) {
            if (err) {
                console.log("Error :\n", err)
                res.status(500).send({ code: 500, content: null, message: "Internal Server Error", error: err })
                return;
            }
            console.log("company name", cName, "\t \t", body);
            body = JSON.parse(body);

            if (body.cod === 200) {
                //********* Insert Into DB */
                const Item = new data({
                    id: body.coord,
                    name: body.main,
                    c_name: body.name
                })
                await Item.save(function (err, document) {
                    if (err) {
                        console.log("Error \n", err);
                        res.status(500).send({ code: 500, content: null, message: "Internal Server Error", error: err })
                        return;
                    }
                    console.log("Document Added Successfully \n", document)
                    return res.status(200).send({ code: 200, content: body, message: "Successfully Added Weather Data", error: null });

                })
            } else {
                res.status(404).send({code : 404, content : body.message, message : "Error while reteriving weather data", error : null});
            }
        })
    }
    else {
        res.status(200).send({ code: 200, content: "Date is not prime so no date", message : "Date is not prime so no date", error : null})
    }
})



async function isPrime(number) {

    if (number <= 1) {
        return false;
    }
    for (var i = 2; i < number; i++)
        if (number % i === 0)
            return false;

    return true;
}