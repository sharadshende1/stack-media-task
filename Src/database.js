const mongoose = require("mongoose");

let dbModel = mongoose.Schema({
    "id" : {
        type : Object
    },
    "name" : {
        type : Object
    },
    "c_name" : {
        type : String
    }
})

const db = mongoose.model("dbconnection", dbModel, "dbconnection");

module.exports = db;