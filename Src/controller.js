const mongoose = require('mongoose');
mongoose.Promise = global.Promise
mongoose.set('useCreateIndex', true)
mongoose.set('useFindAndModify', false);
const dotenv = require('dotenv');
dotenv.config();

var connect = function () {
    mongoose
        .connect("mongodb+srv://mongodbUser:<Sharad@123>@cluster0.6rdfb.mongodb.net/<dbname>?retryWrites=true&w=majority", {
                useNewUrlParser: true,
                useUnifiedTopology: true
            })
        .then(() => {
            console.log('db connected successfully')
        })
        .catch(console.log)
}
module.exports.mongoose = mongoose
module.exports.connect = connect